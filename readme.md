Readme
======

This is a wordpress project with all files included, just for good measure.
The SQL folder should not be on the live web server, it contains a SQL dump of the necessary tables.

1. use the file in the SQL folder to create (or overwrite, if an empty one exists) the wordpress database
2. change the values on the first two items in the "wp_options" table from http://mcalisters.dev to the appropriate new URL (the URL of the staging site)
3. copy everything except the SQL folder into the public html root.
	* if a fresh install of wordpress already exists, you can just copy the wp-content folder from this repo over top of the existing wp-content folder
	* if this is an install from scratch, rename wp-config-sample.php to wp-config.php and edit it to set correct database values
