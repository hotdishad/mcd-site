// Define JavaScript for each page variation of this experiment.
// .versionA and .versionB are hidden by css default



function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}





// Wait for the DOM to load, then execute the view for the chosen variation.
(function($) {
	var pageVariations = [
	  function() {
	  	$('body').addClass('versionA');
	  	$('.home .versionA').css('display', 'block');
	  },  // Original or default this will be the same as A
	  function() {    // Variation 1: Banner Image
	  	$('body').addClass('versionA');
	    $('.home .versionA').css('display', 'block');
	  },
	  function() {    // Variation 2: Sub-heading Text
	  	$('body').addClass('versionB');
	    $('.home .versionB').css('display', 'block');
	  }
	];	
  	// variations AB testing should be
  	// Execute the chosen view
  	// pageVariations[chosenVariation]

  	// doing with GET url PARAM for now
  	var v = 0,
  		version = getParameterByName('version');
  		if(version)v=version;
  		pageVariations[v]();

  	console.log('version ',v);





// BIO BLOCK INTERACTION
	// show the fist one by default
	$('#bio-text-0').css('display', 'block');
	$('#bio-block-0').addClass('active');

	var $blocks = $('.bio-block');

	$blocks.on('click',function(e){
		var id = this.id.split("bio-block-")[1],
			$txtDiv = $('#bio-text-'+id);
		// hide previous blocks
		$('.bio-text').css('display', 'none');
		$blocks.removeClass('active');
	 	$txtDiv.css('display', 'block');
	 	$(this).addClass('active');
	 	$('html, body').animate({
	        scrollTop: $txtDiv.offset().top - 100
	    }, 1000);
    });
})( jQuery );

