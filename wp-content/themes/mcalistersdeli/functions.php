<?php 

function mycustom_featured_width( ) { return 220; /* Custom featured post image width */ }
add_filter( 'et_pb_blog_image_width', 'mycustom_featured_width');

function mycustom_featured_height( ) { return 160; /* Custom featured post image height */ }
add_filter( 'et_pb_blog_image_height', 'mycustom_featured_height');

function mycustom_featured_size( $image_sizes ) {
  $custom_size = mycustom_featured_width() . 'x' . mycustom_featured_height();
  $image_sizes[$custom_size] = 'et-pb-post-main-image-thumbnail';
  return $image_sizes;
}
add_filter( 'et_theme_image_sizes', 'mycustom_featured_size' );


function wpb_adding_scripts() {
    wp_register_script('custom', get_stylesheet_directory_uri() . '/assets/js/custom.min.js', array('jquery'),'1.1', true);
    wp_enqueue_script('custom');
} 

add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' ); 

/* ========= Gravity Forms UTM code ========== */
function grav_utmsource(){
    $utmval = '';
    if (isset($_GET['utm_source'])) {
        $utmval = $_GET['utm_source'];
    } else if (isset($_SESSION['utm_source'])) {
        $utmval = $_SESSION['utm_source'];   
    }
    return $utmval;
}

function grav_utmcampaign(){
    $utmval = '';
    if (isset($_GET['utm_campaign'])) {
        $utmval = $_GET['utm_campaign'];
    } else if (isset($_SESSION['utm_campaign'])) {
        $utmval = $_SESSION['utm_campaign'];   
    }
    return $utmval;
}


function grav_utmmedium(){
    $utmval = '';
    if (isset($_GET['utm_medium'])) {
        $utmval = $_GET['utm_medium'];
    } else if (isset($_SESSION['utm_medium'])) {
        $utmval = $_SESSION['utm_medium'];   
    }
    return $utmval;
}


function grav_utmcontent(){
    $utmval = '';
    if (isset($_GET['utm_content'])) {
        $utmval = $_GET['utm_content'];
    } else if (isset($_SESSION['utm_content'])) {
        $utmval = $_SESSION['utm_content'];   
    }
    return $utmval;
}


function grav_utmterm(){
    $utmval = '';
    if (isset($_GET['utm_term'])) {
        $utmval = $_GET['utm_term'];
    } else if (isset($_SESSION['utm_term'])) {
        $utmval = $_SESSION['utm_term'];   
    }
    return $utmval;
}


function register_my_session()
{
    session_start();
    add_filter('gform_field_value_UTMSOURCE', 'grav_utmsource');
    add_filter('gform_field_value_UTMCAMPAIGN', 'grav_utmcampaign');
    add_filter('gform_field_value_UTMMEDIUM', 'grav_utmmedium');
    add_filter('gform_field_value_UTMCONTENT', 'grav_utmcontent');
    add_filter('gform_field_value_UTMTERM', 'grav_utmterm');
}
add_action('init', 'register_my_session');

function my_skip_mail($f){
    $submission = WPCF7_Submission::get_instance();
    return true; // DO NOT SEND E-MAIL
}
add_filter('wpcf7_skip_mail','my_skip_mail');

//// define the wpcf7_submit callback 
//function action_wpcf7_submit( $instance, $result ) { 
//    do_action( 'wpcf7_mail_sent', $contact_form ); 
//}; 
//         
//// add the action 
//add_action( 'wpcf7_submit', 'action_wpcf7_submit', 10, 2 ); 


?>