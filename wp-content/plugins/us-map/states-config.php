<?php 
//tcerqua@hotdishad.com's edited, less-stupid-than-the-original version
$wp_map = $this->options;

$statesWithLocations = "['iso_" . implode("','iso_",explode(",",strtolower("" . get_field('states_with_locations','options')))) . "']";
$growthStates = "['iso_" . implode("','iso_",explode(",",strtolower("" . get_field('growth_states','options')))) . "']";
$futureStates = "['iso_" . implode("','iso_",explode(",",strtolower("" . get_field('future_growth_states','options')))) . "']";

$states_with_locations_color = get_field('states_with_locations_color','options');
$states_without_locations_color = get_field('states_without_locations_color','options');
$growth_states_dot_color = get_field('growth_states_dot_color','options');
$future_growth_states_dot_color = get_field('future_growth_states_dot_color','options');
$hover_color = get_field('hover_color','options');

$isos = explode(",","iso_xx,iso_al,iso_ak,iso_az,iso_ar,iso_ca,iso_co,iso_ct,iso_de,iso_fl,iso_ga,iso_hi,iso_id,iso_il,iso_in,iso_ia,iso_ks,iso_ky,iso_la,iso_me,iso_md,iso_ma,iso_mi,iso_mn,iso_ms,iso_mo,iso_mt,iso_ne,iso_nv,iso_nh,iso_nj,iso_nm,iso_ny,iso_nc,iso_nd,iso_oh,iso_ok,iso_or,iso_pa,iso_ri,iso_sc,iso_sd,iso_tn,iso_tx,iso_ut,iso_vt,iso_va,iso_wa,iso_wv,iso_wi,iso_wy,iso_dc");
?>

var st_config = {
	'default':{'bordercolor':'<?php echo str_replace("'", "\'",$wp_map['border_color']); ?>','namescolor':'<?php echo str_replace("'", "\'",$wp_map['short_names']); ?>','shadowcolor':'<?php echo str_replace("'","\'",$wp_map['shadow_color']); ?>','lakesfill':'<?php echo str_replace("'", "\'",$wp_map['lake_fill']); ?>','lakesoutline':'<?php echo str_replace("'", "\'",$wp_map['lake_outline']); ?>'},
    <?php
    for($i = 1; $i <= 51; $i++){
        $statebgcolor = $states_without_locations_color;
        if (strpos($statesWithLocations,$isos[$i])) {
            $statebgcolor = $states_with_locations_color;
        }
        $confstr = '';
        $confstr .= "'st_" . $i . "':{'hover': '" . str_replace(array("\n","\r","\r\n","'"),"", is_array($wp_map['hover_content_' . $i]) ? array_map('stripslashes_deep', $wp_map['hover_content_' . $i]) : stripslashes($wp_map['hover_content_' . $i])) . "',";
        $confstr .= "'url':'" . str_replace("'", "\'",$wp_map['url_' . $i]) . "',";
        $confstr .= "'target':'" . str_replace("'", "\'",$wp_map['open_url_' . $i]) . "',";
        $confstr .= "'upcolor':'" . $statebgcolor . "',";
        $confstr .= "'overcolor':'" . $hover_color . "',";
        $confstr .= "'downcolor':'" . $hover_color . "',";
        //$confstr .= "'upcolor':'" . str_replace("'", "\'",$wp_map['up_color_' . $i]) . "',\n";
        //$confstr .= "'overcolor':'" . str_replace("'", "\'",$wp_map['over_color_' . $i]) . "',\n";
        //$confstr .= "'downcolor':'" . str_replace("'", "\'",$wp_map['down_color_' . $i]) . "',\n";
        $confstr .= "'enable':";
        if(!array_key_exists('enable_region_' . $i, $wp_map) || $wp_map['enable_region_' .$i] != '1'){ 
            $confstr .= "false,"; 
        } else {
            $confstr .= "true,"; 
        }
        $confstr .= "'iso':'" . $isos[$i] . "',},\n";
        echo $confstr;
    }
    ?>
}

<?php 
/******** the following javascript places dots on the map. 
NOTE: it requires this style, adjust as needed
#text-abb svg circle { r: 7; stroke: white; stroke-width: 1; pointer-events: none; }
********/
?>
jQuery(window).bind("load", function () {
    var newSVG = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    newSVG.setAttribute("height","100%");
    newSVG.setAttribute("width","100%");
    jQuery('#text-abb').append(newSVG);
    for (var a = 1; 52 > a; a++) {
        stateabbrev = st_config["st_" + a].iso.replace("iso_", "");
        dotstate = !1;
        0 <= jQuery.inArray(st_config["st_" + a].iso, <?=$growthStates?>) ? dotstate = "<?=$growth_states_dot_color?>" : 0 <= jQuery.inArray(st_config["st_" + a].iso, <?=$priorityStates?>) ? dotstate = "" : 0 <= jQuery.inArray(st_config["st_" + a].iso, <?=$futureStates?>) && (dotstate = "<?=$future_growth_states_dot_color?>");
        if (dotstate) { // && !stateabbrev.match("vt|nh|ma|ri|ct|nj|de|md|dc|hi")
            st_config["st_" + a].enable = !0;
            var b = jQuery("#" + st_config["st_" + a].iso).attr("transform").match(/\d+/g).slice(-2);
            var theiso = jQuery("#" + st_config["st_" + a].iso);
            if ( !stateabbrev.match("vt|nh|ma|ri|ct|nj|de|md|dc|hi") ) {
                theiso.replaceWith('');
            }            
            cx = Number(b[0]) + 8;
            cy = Number(b[1]) - 5;
            var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
            circle.setAttribute("cx",cx);
            circle.setAttribute("cy",cy);
            circle.setAttribute("fill",dotstate);
            circle.setAttribute("id","st_" + a);
            circle.setAttribute("r",7);
            jQuery(newSVG).append(circle);
            if ( stateabbrev.match("vt|nh|ma|ri|ct|nj|de|md|dc|hi") ) {
                theiso.attr("font-size","10");
                theiso.attr("stroke","#aaaaaa");
                theiso.attr("stroke-width","2");
                jQuery(newSVG).append(theiso);
                var theiso2 = theiso.clone();
                theiso2.attr("stroke","");
                theiso2.attr("stroke-width","0");
                jQuery(newSVG).append(theiso2);
            }
        }
        if( !stateabbrev.match("vt|nh|ma|ri|ct|nj|de|md|dc|hi") ) {
            jQuery("#" + st_config["st_" + a].iso.toString()).remove();
        } //"" == overlayval && stateabbrev.match("vt|nh|ma|ri|ct|nj|de|md|dc|hi") || jQuery("#" + st_config["st_" + a].iso).replaceWith(overlayval)
    }
    jQuery(document).on("change", ".mobilemapselect", function () {
        this.value && (jQuery("div.mobileselectedinfo").innerHTML = "", st_config["st_" + this.selectedIndex].enable ?
            document.getElementById("mobileselectedinfo").innerHTML = st_config["st_" + this.selectedIndex].hover : document.getElementById("mobileselectedinfo").innerHTML = '<br/><h4 style="text-align: center;">Sorry! This state is not currently an active growth market.</h4>')
    })
});
